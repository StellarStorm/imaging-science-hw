clear
clc
load('nnWorkspace.mat','y1','x1','trainedMean', 'trainedVariance','trainedScale', 'trainedOffset')

% Batch normalization
myepsilon =  1.0000e-05;
myBN   = batchnorm(y1, trainedMean, trainedVariance, myepsilon, trainedScale, trainedOffset);

% Relu
myrelu = relu(myBN);

% max pool
mymaxpool = maxpool(x1);

function bn = batchnorm(img, mean, var, eps, scale, offset)
% Batch Normalization (BLAS 3)
    x_new = (img - mean) / sqrt(var - eps);
    x_scaled = scale*x_new + offset;
    bn = x_scaled;
end


function activation = relu(img)
% RELU activation function
    activation = max(img, 0);
end

function mp = maxpool(img)
% Max Pooling operation for 3D matrices
    pool = zeros(size(img) / 2);
    for m = 1:size(img, 1) / 2
        for n = 1:size(img, 2) / 2
            for p = 1:size(img, 3) / 2
                pool(m, n, p) = max([img(2*m, 2*n, 2*p), 
                                    img(2*m, 2*n, 2*p-1),
                                    img(2*m, 2*n-1, 2*p),
                                    img(2*m, 2*n-1, 2*p-1),
                                    img(2*m-1, 2*n, 2*p),
                                    img(2*m-1, 2*n, 2*p-1),
                                    img(2*m-1, 2*n-1, 2*p),
                                    img(2*m-1, 2*n-1, 2*p-1)]);
            end
        end
    end
    mp = pool;
end