x1=[5;8];
x2=[5;8;1;7];
x3=[5;8;7];
onenorm_x1 = pnorm(x1, 1);
twonorm_x1 = pnorm(x1, 2);
infnorm_x1 = pnorm(x1, Inf);
onenorm_x2 = pnorm(x2, 1);
twonorm_x2 = pnorm(x2, 2);
infnorm_x2 = pnorm(x2, Inf);
onenorm_x3 = pnorm(x3, 1);
twonorm_x3 = pnorm(x3, 2);
infnorm_x3 = pnorm(x3, Inf);

function n = pnorm(x, p)
    if p == Inf
        n = max(abs(x));
    else
        n = sum(x.^p).^(1/p);
    end
end