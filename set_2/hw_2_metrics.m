clc;
clear;
vol1 = double(niftiread('ICBMlores.nii.gz'));
vol2 = double(niftiread('brainT1clores.nii.gz'));
nbins = 32;

MSQ = msq(vol1, vol2);
NCC = ncc(vol1, vol2);
MI = mutualinfo(vol1, vol2, nbins);

function score = msq(A, B)
% MSQ Calculates the Mean Square Error between two images
% 
%   score = msq(A, B) finds the mean squared error between two matrices A
%   and B
    score = sum((A(:) - B(:)).^2) / numel(A);
end

function score = ncc(A, B)
% NCC Calculates the Normalized Cross Correlation between two images
%
%   score = ncc(A, B) finds the  cross correlation between two matrics A
%   and B, normalized within the range [-1, 0]
    A_prime = A(:) - mean(A(:));
    B_prime = B(:) - mean(B(:));
    
    num = dot(A_prime, B_prime)^2;
    
    A_norm = sqrt(sum(A_prime.^2));
    B_norm = sqrt(sum(B_prime.^2));
    
    score = - num /(A_norm^2 * B_norm^2);
end

function p = probabilities(A, nbins)
% PROBABILITIES Calculates the probabilities of an image
% 
% p = probabilities(A, nbins) finds the 1D histogram of the probabilities
% of pixel values of A falling into nbins-many equally spaced bins,
% normalized for the range [0, 1]
    eps = 1e-14;  % Small correction factor to avoid NaN
    A = A(:);
    img_min = min(A);
    img_max = max(A);
    binsize = (img_max - img_min) / nbins;
    
    histogram = zeros(nbins+1, 1);
    for i = 1:length(A)
        idx = floor((A(i) - img_min) / binsize) + 1;
        histogram(idx) = histogram(idx) + 1;
    end
    p = histogram / length(A); % Normalize to 1
    p = p + eps;
end

function p = joint_probabilities(A, B, nbins)
% JOINT_PROBABILITIES Calculates the joint probabilities of two images
% 
%   p = join_probabilities(A, B, nbins) returns a 2D histogram of
%   probabilities, normalized over the range [0, 1] and grouped into nbins
%   bins, for the two same-dimension matrices A and B
    eps = 1e-14;
    A = A(:);
    B = B(:);
    a_min = min(A);
    a_max = max(A);
    b_min = min(B);
    b_max = max(B);

    a_binsize = (a_max - a_min) / nbins;
    b_binsize = (b_max - b_min) / nbins;
    
    histogram = zeros(nbins+1, nbins+1);
    for i = 1:length(A)
        idx1 = floor((A(i) - a_min) / a_binsize) + 1;
        idx2 = floor((B(i) - b_min) / b_binsize) + 1;
        
        histogram(idx1, idx2) = histogram(idx1, idx2) + 1;
    end
    p = histogram / length(A);
    p = p + eps;
end

function h  = entropy(probs)
% ENTROPY Calculates the (Shannon) entropy of a 1-D probability
% distribution
%
%   h = entropy(probs) is the shannon entropy of a 1D array of
%   probabilities, like a histogram. The array should be normalized to the
%   range [0, 1]
    h = -sum(probs .*log(probs));
end

function mi = mutualinfo(A, B, nbins)
% MUTUALINFO Calculates the mutual information of two same-dimension images
% 
%   mi = mutualinfo(A, B, nbims) is the mutual information of matrics A
%   and B, using nbins for each matrix
    HA = entropy(probabilities(A, nbins));
    HB = entropy(probabilities(B, nbins));
    HAB = sum(entropy(joint_probabilities(A, B, nbins)));
    mi = HA + HB - HAB;
end
