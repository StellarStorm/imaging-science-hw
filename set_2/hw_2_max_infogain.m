% format long
clear;
clc;
%Load images
IntensityImage = niftiread('ICBMlores.nii.gz');
LabelImage = niftiread('ICBMGMWMCSFlores.nii.gz');
%Extract non-background data
labelmask = LabelImage(:);
intensitydata = IntensityImage(:);
labelsubset = labelmask(labelmask>0);
intensitysubset = intensitydata(labelmask>0);

% compute the intial entropy
Initial = [sum(labelsubset == 1), sum(labelsubset == 2), sum(labelsubset == 3)];
pdfInitial = 1 / sum(Initial) * Initial;
EntropyBefore = -sum(pdfInitial .*log(pdfInitial));

[iimg_sort, iimg_idx] = sort(IntensityImage, 'ascend');
limg_sort = LabelImage(iimg_idx);
[isub_sort, isub_idx] = sort(intensitysubset, 'ascend');
lsub_sort = labelsubset(isub_idx);

max_gain = -Inf;
max_iter = 1;
max_thresh = 1;
for i = round(min(intensitysubset)):round(max(intensitysubset))
    thresh = i;
    
    left_img = isub_sort(isub_sort < thresh);
    right_img = isub_sort(isub_sort >= thresh);
    left_lab = lsub_sort(1:numel(left_img));
    right_lab = lsub_sort(numel(left_img)+1:end);

    GREYCSF = [sum(left_lab == 1), sum(left_lab == 2), sum(left_lab == 3)];
    WHITE = [sum(right_lab == 1), sum(right_lab == 2), sum(right_lab == 3)];

    % verify = sum(GREYCSF) + sum(WHITE) - sum(Initial)

    % compute entropies after split
    pdfGREYCSF = 1 / sum(GREYCSF) * GREYCSF;
    pdfWHITE = 1 / sum(WHITE) * WHITE;
    EntropyGREYCSF = -sum(pdfGREYCSF .*log(pdfGREYCSF));
    EntropyWHITE = -sum(pdfWHITE .*log(pdfWHITE));

    % each split is normalized by the number of entries
    EntropyAfter = sum(GREYCSF) / sum(Initial) * EntropyGREYCSF + ...
        sum(WHITE) / sum(Initial) * EntropyWHITE;

    InformationGain = EntropyBefore - EntropyAfter;
    
    if InformationGain > max_gain
        max_gain = InformationGain;
        max_iter = i;
        max_thresh = thresh;
    end
end

intensityMaxInformationGain = max_thresh;
maxInformationGain = max_gain;