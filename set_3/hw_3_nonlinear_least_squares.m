clear;
clc;
% first iteration of Guass Newton Approach
InitialGuess = [1;1;1];
myShortAnswer1 = 'The maximum of the distances between each guess and the current values';
[res, jac] = pharmacokinetic(InitialGuess);

% Compute initial residual
InitialResidual = res;

% Compute Least Square Solution
LeastSquareSolution = -jac\res;
% Compute Normal Eqn Solution
NormalEqnSoln = (jac' * jac)\(jac' * -res);
% Compute QR Solution
[q, r] = qr(jac);
QRSoln = r\(q' * -res);
myShortAnswer2 = 'In this case I did not observe any differences. However, in general, QR factorization is advantageous because it preserves the norm'; 

% % hand coded Guass Newton solver
% % Keeping this for historical purposes - some of these couldn't be changed
% % in the MATLAB Grader assignment. I prefer the second solver below
% convergence = 1.0;
% tolerance = 7.e-2;                                                                                    
% x = InitialGuess;
% while (convergence > tolerance)
%   % update solution
%   [res, jac] = pharmacokinetic(x);
%   step = -jac\res;
%   x_old = x;
%   % only take half the step length                                                                    
%   x = x+ 0.5* step;
%   convergence = max(abs(x_old - x));
% end

% hand coded Guass Newton solver - #2, closer to book example
convergence = 1/2*(res'*res);
tolerance = 7.e-2;                                                                                    
x = QRSoln;
count = 0;
while (convergence > tolerance)
  [res, jac] = pharmacokinetic(x);
  x_new = x + -jac\res;
  convergence = max(abs(x - x_new));
  % update solution
  x = x_new;
  count = count + 1;
end

myShortAnswer3 = 'The Gauss-Newton method (an approximation to the Hessian) is a Quasi-Newton method. I chose this in order to take advantage of the quadratic convergence of the Newton method while being more numerically stable'; 
% 
% lsqnonlin(@pharmacokinetic,QRSoln,-inf,inf,optimset('jacobian','off'))
% lsqnonlin(@pharmacokinetic,QRSoln,-inf,inf,optimset('jacobian','on'))
myShortAnswer4 = "Convergence rate is observed to be approximately quadratic (but only near the solution). I noticed that it is significantly faster than lsqnonlin if the jacobian isn't used, but approximately the same (8 vs. 9 iterations) if the jacobian is specified.";

function [residual, jacobian] = pharmacokinetic(x)
% Modified rrom Dr. Fuentes' book, p. 125
time = [0.0; 1.0; 2.0; 3.0; 4.0];
y = [3.0; 2.7; 1.3; 0.7; 0.1];
residual = y - x(1)* exp(x(3)*time +x(2)*time.*time);
jacobian = [-exp(x(3)*time +x(2)*time.*time), ...
  -x(1)*time.*time.*exp(x(3)*time +x(2)*time.*time), ...
  -x(1)*time.*exp(x(3)*time +x(2)*time.*time)];
disp(sprintf('%f %f %f %f', x(1), x(2), x(3), residual'*residual));
end
