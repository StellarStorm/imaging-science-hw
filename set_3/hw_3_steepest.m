% Consider the quadratic function
% $$min_x f(x) = \frac{1}{2} x^T Ax - b^T x$$ with $x \in R^{2x2}$
% symmetric $b \in R^2$
% 
% - What are the eigen-values and eigen-vectors of $A$ ?
% - Code the Steepest Descent Method Algorithm with exact line search and
% apply it to find a solution
% $$x_{k+1} = x{k} - \frac{\nabla f_k^t \nabla f_k}{\nabla f_l^T A \nabla
% f_k} \nabla f-k$$
%
%
clear;
clc;
A = [8, 2;2,900];
b = [6;1];
x = [0;0];

f = (1/2 * x'*A*x) - (b'*x);
fprime = A*x - b;
% evaluate the eigen values as an array
syms l
E = A - l*eye(2,2);
eivals = vpa(solve(det(E) == 0, l));
myEigenValues  = double([eivals(1), eivals(2)]);

convergence = 1.0;
tolerance   = 1.e-12;
iterations  = 0;
% update the variable x with your solution
while (convergence > tolerance)
  % evaluate exact line search 
  f = (1/2 * x'*A*x) - (b'*x);
  fprime = A*x - b;
  alpha = (fprime' * fprime)/(fprime' *A* fprime);
  % update the solution
  x_new = x - alpha*fprime;
  % update the iteration count
  convergence = abs(x - x_new);
  x = x_new;
  iterations  = iterations  + 1;

end
disp(sprintf('# iter = %d, x = (%f,%f)',iterations, x));
